package lafa;

import java.io.Serializable;

import edu.jas.arith.BigRational;

// helper class for ordered octonion triples
public class BigVectorOctonionTriple implements Serializable {

	private static final long serialVersionUID = 1L;
	private BigVectorOctonion[] o;
	private int hashCode;

	BigVectorOctonionTriple(BigVectorOctonion o1, BigVectorOctonion o2,
			BigVectorOctonion o3) {
		o = new BigVectorOctonion[3];
		this.o[0] = o1;
		this.o[1] = o2;
		this.o[2] = o3;
		this.hashCode = o[0].hashCode() + o[1].hashCode() + o[2].hashCode();
	}

	BigVectorOctonionTriple(BigVectorOctonionTriple initO) {
		this.o = initO.get();
		this.hashCode = o[0].hashCode() + o[1].hashCode() + o[2].hashCode();
	}

	public BigVectorOctonion[] get() {
		return o;
	}

	public BigVectorOctonion get(int index) {
		return o[index];
	}

	public BigVectorOctonionTriple sum(BigVectorOctonionTriple addend) {
		return new BigVectorOctonionTriple(addend.get(0).sum(o[0]), addend.get(
				1).sum(o[1]), addend.get(2).sum(o[2]));
	}

	public BigVectorOctonionTriple subtract(BigVectorOctonionTriple subtrahend) {
		return new BigVectorOctonionTriple(subtrahend.get(0).subtract(o[0]), subtrahend.get(
				1).subtract(o[1]), subtrahend.get(2).subtract(o[2]));
	}

	public BigVectorOctonionTriple scalarProduct(BigVectorOctonionTriple factor) {
		BigVectorOctonion subProduct = o[0].scalarProduct(factor.get(0))
				.sum(o[1].scalarProduct(factor.get(1)))
				.sum(o[2].scalarProduct(factor.get(2)));

		return new BigVectorOctonionTriple(subProduct, BigVectorOctonion.ZERO,
				BigVectorOctonion.ZERO);
	}

	public BigVectorOctonionTriple polynomialProduct(
			BigVectorOctonionTriple factor) {
		// build three-component product from polynomial ring:
		// { ( x1 x2 + y1 z2 + z1 y2 ),
		// ( x1 y2 + y1 x2 + z1 z2 ),
		// ( x1 z2 + y1 y2 + z1 x2 ) }
		
		BigVectorOctonion vecCompX = null;
		BigVectorOctonion vecCompY = null;
		BigVectorOctonion vecCompZ = null;
		BigVectorOctonion[] f = new BigVectorOctonion[3];
		
		f[0] = factor.get(0);
		f[1] = factor.get(1);
		f[2] = factor.get(2);

		vecCompX = (o[0].multiply(f[0])).sum(o[1].multiply(f[2])).sum(
				o[2].multiply(f[1]));
		vecCompY = (o[0].multiply(f[1])).sum(o[1].multiply(f[0])).sum(
				o[2].multiply(f[2]));
		vecCompZ = (o[0].multiply(f[2])).sum(o[1].multiply(f[1])).sum(
				o[2].multiply(f[0]));

		return new BigVectorOctonionTriple(vecCompX, vecCompY, vecCompZ);

	}

	public BigVectorOctonionTriple conjugate() {
		return new BigVectorOctonionTriple(o[0].conjugate(),
				o[1].multiply(BigVectorOctonion.NEGATIVE_ONE),
				o[2].multiply(BigVectorOctonion.NEGATIVE_ONE));
	}

	public BigVectorOctonionTriple componentConjugate() {
		return new BigVectorOctonionTriple(o[0].conjugate(),
				o[1].conjugate(),
				o[2].conjugate());
	}

	public BigRational norm() {
		return o[0].norm().sum(o[1].norm()).sum(o[2].norm()).getRe();
	}
	
	public BigRational getRe() {
		return o[0].getRe();
	}

	public String toVectorString() {

		return "{ " + o[0].toVectorString() + ", " + o[1].toVectorString()
				+ ", " + o[2].toVectorString() + " }";
	}

	@Override
	public boolean equals(Object compareTo) {

		if (compareTo == null)
			return false;
		if (compareTo.getClass() != BigVectorOctonionTriple.class)
			return false;
		BigVectorOctonionTriple compare = (BigVectorOctonionTriple) compareTo;

		return o[0].equals(compare.get(0)) && o[1].equals(compare.get(1))
				&& o[2].equals(compare.get(2));
	}

	@Override
	public int hashCode() {
		return this.hashCode;
	}

}
