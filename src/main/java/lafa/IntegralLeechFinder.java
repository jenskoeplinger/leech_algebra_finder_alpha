/**
 * @author Jens Koeplinger (2013)
 * Public domain (no copyrights; you would be foolish to copy it ... rewrite would be more prudent)
 *
 * This is lab code, i.e., poorly written, hard to read, undocumented, poor programming conventions,
 * something I use ad-hoc now and then.
 *
 * References:
 *
 * [Wilson] Robert A. Wilson, Octonions and the Leech lattice (2008/2009)
 * [Dixon] Geoffrey M. Dixon, Division Algebras, Lattices, Physics, Windmill Tilting (2011)
 */
package lafa;

import edu.jas.arith.BigRational;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

import lafa.*;

public class IntegralLeechFinder {

	final static boolean RUN_INTERNAL_TESTS = false;
	final static boolean SKIP_INTERNAL_TESTS_FOR_DUPLICATES = true;
	final static boolean STOP_AFTER_TESTS = false;
	final static boolean HEAVY_DEBUG_OUTPUT = false;
	final static boolean CALCULATE_DIXON = false;
	final static boolean CALCULATE_WILSON = true;
	final static boolean WRITE_COMPUTED_LISTS_TO_FILE_CACHE = false;
	final static boolean READ_COMPUTED_LISTS_FROM_FILE_CACHE = false;
	final static boolean SNAPSHOT_DEBUG_OUTPUT = true;
	final static boolean DUMP_RESULT_SET = true;

	static BigVectorOctonion octonionOne;
	static BigVectorOctonion octonionNegativeOne;
	static BigVectorOctonion octonionTwo;
	static BigVectorOctonion[] octonionUnits;
	static BigVectorOctonion[] octonionNegativeUnits;

	// [Wilson] s = (1/2)( - 1 + e_1 + ... + e_7 )
	static BigVectorOctonion s;

	// [Wilson] \overbar{ s } = (1/2)( - 1 - e_1 - ... - e_7 )
	static BigVectorOctonion sBar;

	// [Dixon] l_0 = (1/2)( 1 + e_1 + ... + e_7 )
	static BigVectorOctonion l0;

	// [Dixon] {l_0}* = (1/2)( 1 - e_1 - ... - e_7 )
	static BigVectorOctonion l0Bar;

	// E8 lattice inner shell (in [Dixon] A^even)
	static ArrayList<BigVectorOctonion> aEven;

	// E8 lattice inner shell (in [Dixon] A^odd)
	static ArrayList<BigVectorOctonion> aOdd;

	// Leech lattice inner shell [Dixon]
	static ArrayList<BigVectorOctonionTriple> leechDixon;

	// E8 lattice inner shell (in [Wilson] L)
	static ArrayList<BigVectorOctonion> e8L;

	// e8L * s
	static ArrayList<BigVectorOctonion> e8L_s;

	// e8L * \overbar{ s }
	static ArrayList<BigVectorOctonion> e8L_sBar;

	// e8L * 2
	static ArrayList<BigVectorOctonion> e8L_2;

	// ( e8L * s ) * (+- e_i) where e_i octonion basis i = 0...7
	static ArrayList<BigVectorOctonion> e8L_s_j;

	// ( e8L * sBar ) * (+- e_i)
	static ArrayList<BigVectorOctonion> e8L_sBar_j;

	// e8L * (+- e_i)
	static ArrayList<BigVectorOctonion> e8L_j;

	// ( e8L * (+- e_i) ) * (+-e_i)
	static ArrayList<BigVectorOctonion> e8L_j_k;

	// Leech lattice inner shell [Wilson]
	static ArrayList<BigVectorOctonionTriple> leechWilson;

	static final int RANDOM_TESTS_COUNT = 10000;
	static final int SCALAR_PRODUCT_TESTS_COUNT = 10000;

	//
	// ---------------------------------------------------------
	// MAIN
	// ---------------------------------------------------------
	//

	public static void main(String[] args) throws Exception {

		// initialize the helper lists (Leech lattice, E8 lattice, etc)
		initialize();

		// test the program itself:
		if (RUN_INTERNAL_TESTS) {
			runInternalTests();
		} else {
			H.sopln("Skipping internal tests per RUN_INTERNAL_TESTS flag");
		}

		if (STOP_AFTER_TESTS) {
			H.sopln("Stopping after tests per STOP_AFTER_TESTS flag.");
			return;
		}

		H.sopln("Start computation of products");
		// ======
		// ACTUAL COMPUTATIONS COMES HERE.

		// Using Wilson L test exceptional Jordan product repd as 3x3 self-adjoint matrix,
		// map Leech octonion triples into matrix position, and set diagonal to zero.
		H.sopln("Test exceptional Jordan product repd as 3x3 self-adjoint matrix,");
		H.sopln("map Wilson Leech octonion triples into matrix position, and set diagonal to zero.");
		H.sopln("Test for closure.");
		long foundCountBinary = 0;
		long foundCountTernary = 0;
		long count = 0;

		for (count = 1; count > 0; count++) { // forever

			// randomly select three points from the Leech lattice
			int idx1 = (int) (Math.random() * 196560.);			
			int idx2 = (int) (Math.random() * 196560.);
			int idx3 = (int) (Math.random() * 196560.);
			
			BigVectorOctonionTriple lw1 = leechWilson.get(idx1);
			BigVectorOctonionTriple lw2 = leechWilson.get(idx2);
			BigVectorOctonionTriple lw3 = leechWilson.get(idx3);
			
			// extract the octonionic triples using Wilson's construction 			
			BigVectorOctonion lw1_0 = lw1.get(0);
			BigVectorOctonion lw1_1 = lw1.get(1);
			BigVectorOctonion lw1_2 = lw1.get(2);
			
			BigVectorOctonion lw2_0 = lw2.get(0);
			BigVectorOctonion lw2_1 = lw2.get(1);
			BigVectorOctonion lw2_2 = lw2.get(2);
			
			BigVectorOctonion lw3_0 = lw3.get(0);
			BigVectorOctonion lw3_1 = lw3.get(1);
			BigVectorOctonion lw3_2 = lw3.get(2);
			
			// Build exceptional Jordan product using 3x3 octonionic matrix multiplication. Matrix position labelling:
			//
			//   / a b c \
			//   | d e f |
			//   \ g h i /
			//
			// Mapping rules:
			// - Leave diagonal { a, e, i } zero.
			// - Put octonion _0 on { d } and complex conjugate on { b }
			// - Put octonion _1 on { c } and complex conjugate on { g }
			// - Put octonion _2 on { h } and complex conjugate on { f }
			//
			// Test: Multiply matrices and test whether the product's octonion triple on  { d, g, h } is in the Leech lattice.
			
			BigVectorOctonion lw1_d = lw1_0;
			BigVectorOctonion lw1_b = lw1_0.conjugate();
			BigVectorOctonion lw1_g = lw1_1;
			BigVectorOctonion lw1_c = lw1_1.conjugate();
			BigVectorOctonion lw1_h = lw1_2;
			BigVectorOctonion lw1_f = lw1_2.conjugate();
			
			BigVectorOctonion lw2_d = lw2_0;
			BigVectorOctonion lw2_b = lw2_0.conjugate();
			BigVectorOctonion lw2_g = lw2_1;
			BigVectorOctonion lw2_c = lw2_1.conjugate();
			BigVectorOctonion lw2_h = lw2_2;
			BigVectorOctonion lw2_f = lw2_2.conjugate();
			
			BigVectorOctonion lw3_d = lw3_0;
			BigVectorOctonion lw3_b = lw3_0.conjugate();
			BigVectorOctonion lw3_g = lw3_1;
			BigVectorOctonion lw3_c = lw3_1.conjugate();
			BigVectorOctonion lw3_h = lw3_2;
			BigVectorOctonion lw3_f = lw3_2.conjugate();
			
			// Build Jordan product from Jordan products:
			// lw123 = (lw1 . lw2) . lw3 + (lw2 . lw3) . lw1 - (lw3 . lw1) . lw3
			// where . is the Jordan product lw1 . lw2 := 1/2(lw1 * lw2)
			//
			// Then test whether closed on Leech.
			
			// First the three terms inside the brackets ...
			BigVectorOctonion lw12_d = lw1_f.multiply(lw2_g);
			BigVectorOctonion lw12_g = lw1_h.multiply(lw2_d);
			BigVectorOctonion lw12_h = lw1_g.multiply(lw2_b);
			BigVectorOctonion lw12_f = lw12_h.conjugate(); 
			BigVectorOctonion lw21_d = lw2_f.multiply(lw1_g);
			BigVectorOctonion lw21_g = lw2_h.multiply(lw1_d);
			BigVectorOctonion lw21_h = lw2_g.multiply(lw1_b);
			BigVectorOctonion lw21_f = lw21_h.conjugate(); 
			BigVectorOctonion lw1221_d = lw12_d.sum(lw21_d); 
			BigVectorOctonion lw1221_g = lw12_g.sum(lw21_g);
			BigVectorOctonion lw1221_h = lw12_h.sum(lw21_h);
			BigVectorOctonion lw1221_f = lw12_f.sum(lw21_f);
			BigVectorOctonion lw1221_b = lw1221_d.conjugate();

			BigVectorOctonion lw23_d = lw2_f.multiply(lw3_g);
			BigVectorOctonion lw23_g = lw2_h.multiply(lw3_d);
			BigVectorOctonion lw23_h = lw2_g.multiply(lw3_b);
			BigVectorOctonion lw23_f = lw23_h.conjugate();
			BigVectorOctonion lw32_d = lw3_f.multiply(lw2_g);
			BigVectorOctonion lw32_g = lw3_h.multiply(lw2_d);
			BigVectorOctonion lw32_h = lw3_g.multiply(lw2_b);
			BigVectorOctonion lw32_f = lw32_h.conjugate();
			BigVectorOctonion lw2332_d = lw23_d.sum(lw32_d); 
			BigVectorOctonion lw2332_g = lw23_g.sum(lw32_g);
			BigVectorOctonion lw2332_h = lw23_h.sum(lw32_h);
			BigVectorOctonion lw2332_f = lw23_f.sum(lw32_f);
			BigVectorOctonion lw2332_b = lw2332_d.conjugate();

			BigVectorOctonion lw31_d = lw3_f.multiply(lw1_g);
			BigVectorOctonion lw31_g = lw3_h.multiply(lw1_d);
			BigVectorOctonion lw31_h = lw3_g.multiply(lw1_b);
			BigVectorOctonion lw31_f = lw31_h.conjugate();
			BigVectorOctonion lw13_d = lw1_f.multiply(lw3_g);
			BigVectorOctonion lw13_g = lw1_h.multiply(lw3_d);
			BigVectorOctonion lw13_h = lw1_g.multiply(lw3_b);
			BigVectorOctonion lw13_f = lw13_h.conjugate();
			BigVectorOctonion lw3113_d = lw31_d.sum(lw13_d); 
			BigVectorOctonion lw3113_g = lw31_g.sum(lw13_g);
			BigVectorOctonion lw3113_h = lw31_h.sum(lw13_h);
			BigVectorOctonion lw3113_f = lw31_f.sum(lw13_f);
			BigVectorOctonion lw3113_b = lw3113_d.conjugate();

			// ... then the three addends ...
			BigVectorOctonion lw1221x3_d = lw1221_f.multiply(lw3_g);
			BigVectorOctonion lw1221x3_g = lw1221_h.multiply(lw3_d);
			BigVectorOctonion lw1221x3_h = lw1221_g.multiply(lw3_b);
			BigVectorOctonion lw3x1221_d = lw3_f.multiply(lw1221_g);
			BigVectorOctonion lw3x1221_g = lw3_h.multiply(lw1221_d);
			BigVectorOctonion lw3x1221_h = lw3_g.multiply(lw1221_b);
			BigVectorOctonion lw1x2x3_d = lw1221x3_d.sum(lw3x1221_d);
			BigVectorOctonion lw1x2x3_g = lw1221x3_g.sum(lw3x1221_g);
			BigVectorOctonion lw1x2x3_h = lw1221x3_h.sum(lw3x1221_h);

			BigVectorOctonion lw2332x1_d = lw2332_f.multiply(lw1_g);
			BigVectorOctonion lw2332x1_g = lw2332_h.multiply(lw1_d);
			BigVectorOctonion lw2332x1_h = lw2332_g.multiply(lw1_b);
			BigVectorOctonion lw1x2332_d = lw1_f.multiply(lw2332_g);
			BigVectorOctonion lw1x2332_g = lw1_h.multiply(lw2332_d);
			BigVectorOctonion lw1x2332_h = lw1_g.multiply(lw2332_b);
			BigVectorOctonion lw2x3x1_d = lw2332x1_d.sum(lw1x2332_d);
			BigVectorOctonion lw2x3x1_g = lw2332x1_g.sum(lw1x2332_g);
			BigVectorOctonion lw2x3x1_h = lw2332x1_h.sum(lw1x2332_h);

			BigVectorOctonion lw3113x2_d = lw3113_f.multiply(lw2_g);
			BigVectorOctonion lw3113x2_g = lw3113_h.multiply(lw2_d);
			BigVectorOctonion lw3113x2_h = lw3113_g.multiply(lw2_b);
			BigVectorOctonion lw2x3113_d = lw2_f.multiply(lw3113_g);
			BigVectorOctonion lw2x3113_g = lw2_h.multiply(lw3113_d);
			BigVectorOctonion lw2x3113_h = lw2_g.multiply(lw3113_b);
			BigVectorOctonion lw3x1x2_d = lw3113x2_d.sum(lw2x3113_d);
			BigVectorOctonion lw3x1x2_g = lw3113x2_g.sum(lw2x3113_g);
			BigVectorOctonion lw3x1x2_h = lw3113x2_h.sum(lw2x3113_h);

			// ... then adding / subtracting them to form the Jordan triple:
			BigVectorOctonion lw123_d = lw1x2x3_d.sum(lw2x3x1_d).subtract(lw3113x2_d);					
			BigVectorOctonion lw123_g = lw1x2x3_g.sum(lw2x3x1_g).subtract(lw3113x2_g);
			BigVectorOctonion lw123_h = lw1x2x3_h.sum(lw2x3x1_h).subtract(lw3113x2_h);			
			
			BigVectorOctonionTriple lw123_test = new BigVectorOctonionTriple(lw123_d, lw123_g, lw123_h);
			
			if (HEAVY_DEBUG_OUTPUT
					|| (SNAPSHOT_DEBUG_OUTPUT && ((count % 100) == 0))) {
				H.sopln("test #" + count + ", "
						+ "found so far: binary " + foundCountBinary + ", "
						+ "triple " + foundCountTernary + ", "
						+ "this test:");
				H.sopln("  Leech(Wilson) [" + idx1 + "],[" + idx2 + "],[" + idx3 + "]");
				H.sopln("    [" + idx1 + "] = " + lw1.toVectorString());
				H.sopln("    [" + idx2 + "] = " + lw2.toVectorString());
				H.sopln("    [" + idx3 + "] = " + lw3.toVectorString());
				H.sopln("  ternary product test:");
				H.sopln("    ( [" + idx1 + "] * [" + idx2 + "] ) * [" + idx3 + "] = " + lw123_test.toVectorString());
				H.sopln("        in Leech(Wilson): " + (leechWilson.contains(lw123_test)));
			}
		}

		// END OF ACTUAL COMPUTATIONS.
		// ======
		H.sopln("All done. Exiting.");

	}

	private static String printBigRationalHistogram(
			ArrayList<BigRational> thisList) {
		// prints a sorted histogram of entries in the incoming array list
		String returnS = "";
		Collections.sort(thisList);
		BigRational thisRational = thisList.get(0);
		BigRational nextRational;
		int thisCount = 1;
		for (int i = 1; i < thisList.size(); i++) {
			nextRational = thisList.get(i);
			if (nextRational.equals(thisRational)) {
				thisCount++;
			} else {
				returnS += thisRational.toString() + " [" + thisCount + "], ";
				thisCount = 1;
				thisRational = new BigRational(nextRational.toString());
			}
		}
		returnS += thisRational.toString() + " [" + thisCount + "]";
		return returnS;
	}

	//
	// -----------------------------------------------------------
	// Initializations
	// -----------------------------------------------------------
	//

	private static void initialize() throws Exception {

		H.sopln("Initializing");

		octonionOne = new BigVectorOctonion("1");
		octonionNegativeOne = new BigVectorOctonion("-1");
		octonionTwo = new BigVectorOctonion("2");

		octonionUnits = BigVectorOctonion.units;
		octonionNegativeUnits = BigVectorOctonion.negativeUnits;

		s = (new BigVectorOctonion("-1", "1", "1", "1", "1", "1", "1", "1"))
				.divide(octonionTwo);
		H.sopln("s = " + s.toVectorString());

		sBar = (new BigVectorOctonion("-1", "-1", "-1", "-1", "-1", "-1", "-1",
				"-1")).divide(octonionTwo);
		H.sopln("sBar = " + sBar.toVectorString());

		l0 = (new BigVectorOctonion("1", "1", "1", "1", "1", "1", "1", "1"))
				.divide(octonionTwo);
		H.sopln("l0 = " + l0.toVectorString());

		l0Bar = l0.conjugate();
		H.sopln("l0Bar = " + l0Bar.toVectorString());

		if (READ_COMPUTED_LISTS_FROM_FILE_CACHE == false) {
			calculateComputedLists();
			if (WRITE_COMPUTED_LISTS_TO_FILE_CACHE) {
				writeComputedListsToFile();
			}
		} else {
			readComputedListsFromFile();
		}

	}

	private static void writeComputedListsToFile() throws IOException {

		if (CALCULATE_DIXON) {
			H.writeObjectToTextFile(aEven, "cache.aEven");
			H.writeObjectToTextFile(aOdd, "cache.aOdd");
			H.writeObjectToTextFile(leechDixon, "cache.leechDixon");
		}

		if (CALCULATE_WILSON) {
			H.writeObjectToTextFile(e8L, "cache.e8L");
			H.writeObjectToTextFile(e8L_s, "cache.e8L_s");
			H.writeObjectToTextFile(e8L_sBar, "cache.e8L_sBar");
			H.writeObjectToTextFile(e8L_2, "cache.e8L_2");
			H.writeObjectToTextFile(e8L_s_j, "cache.e8L_s_j");
			H.writeObjectToTextFile(e8L_sBar_j, "cache.e8L_sBar_j");
			H.writeObjectToTextFile(e8L_j, "cache.e8L_j");
			H.writeObjectToTextFile(e8L_j_k, "cache.e8L_j_k");
			H.writeObjectToTextFile(leechWilson, "cache.leechWilson");
		}

	}

	private static void readComputedListsFromFile() throws IOException,
			ClassNotFoundException {

		if (CALCULATE_DIXON) {
			aEven = (ArrayList<BigVectorOctonion>) H
					.readObjectFromTextFile("cache.aEven");
			aOdd = (ArrayList<BigVectorOctonion>) H
					.readObjectFromTextFile("cache.aOdd");
			leechDixon = (ArrayList<BigVectorOctonionTriple>) H
					.readObjectFromTextFile("cache.leechDixon");
		}

		if (CALCULATE_WILSON) {
			e8L = (ArrayList<BigVectorOctonion>) H
					.readObjectFromTextFile("cache.e8L");
			e8L_s = (ArrayList<BigVectorOctonion>) H
					.readObjectFromTextFile("cache.e8L_s");
			e8L_sBar = (ArrayList<BigVectorOctonion>) H
					.readObjectFromTextFile("cache.e8L_sBar");
			e8L_2 = (ArrayList<BigVectorOctonion>) H
					.readObjectFromTextFile("cache.e8L_2");
			e8L_s_j = (ArrayList<BigVectorOctonion>) H
					.readObjectFromTextFile("cache.e8L_s_j");
			e8L_sBar_j = (ArrayList<BigVectorOctonion>) H
					.readObjectFromTextFile("cache.e8L_sBar_j");
			e8L_j = (ArrayList<BigVectorOctonion>) H
					.readObjectFromTextFile("cache.e8L_j");
			e8L_j_k = (ArrayList<BigVectorOctonion>) H
					.readObjectFromTextFile("cache.e8L_j_k");
			leechWilson = (ArrayList<BigVectorOctonionTriple>) H
					.readObjectFromTextFile("cache.leechWilson");
		}

	}

	private static void calculateComputedLists() throws Exception {

		int cnt;
		int numMinus;
		String binaryString;
		BigVectorOctonion thisOctonion;

		// build E8 lattices

		if (CALCULATE_DIXON) {

			H.sopln("Generating E8 lattice inner shell (Dixon A^even)");
			aEven = new ArrayList<BigVectorOctonion>();

			// (1) get +-e_i
			cnt = 0;
			for (int i = 0; i < 8; i++) {
				aEven.add(octonionUnits[i]);
				aEven.add(octonionNegativeUnits[i]);

				if ((SNAPSHOT_DEBUG_OUTPUT && ((i % 5) == 0))
						|| HEAVY_DEBUG_OUTPUT) {
					if (SNAPSHOT_DEBUG_OUTPUT)
						H.sopln("Dixon A^even Snapshot " + (cnt + 1) + "/240:");
					H.sopln("  " + aEven.get(cnt).toVectorString());
					H.sopln("  " + aEven.get(cnt + 1).toVectorString());
				}

				cnt += 2;

			}

			if (cnt != 16)
				throw new Exception(
						"Generation of E8 lattice inner shell failed expected members interim test. Check code.");

			// (2) get (1/2)(+-e_i+-e_j+-e_k+-e_l)
			// where { i,j,k,l } distinct
			// and e_i(e_j(e_k e_l)) = +- 1
			for (int i = 0; i < 5; i++) {
				for (int j = (i + 1); j < 6; j++) {
					for (int k = (j + 1); k < 7; k++) {
						for (int l = (k + 1); l < 8; l++) {

							BigVectorOctonion prod = octonionUnits[i]
									.multiply(octonionUnits[j])
									.multiply(octonionUnits[k])
									.multiply(octonionUnits[l]);

							if (prod.equals(octonionOne)
									|| prod.equals(octonionNegativeOne)) {
								// add all plus/minus permutations

								aEven.add(octonionUnits[i]
										.sum(octonionUnits[j])
										.sum(octonionUnits[k])
										.sum(octonionUnits[l])
										.divide(octonionTwo));
								aEven.add(octonionUnits[i]
										.subtract(octonionUnits[j])
										.sum(octonionUnits[k])
										.sum(octonionUnits[l])
										.divide(octonionTwo));
								aEven.add(octonionUnits[i]
										.sum(octonionUnits[j])
										.subtract(octonionUnits[k])
										.sum(octonionUnits[l])
										.divide(octonionTwo));
								aEven.add(octonionUnits[i]
										.subtract(octonionUnits[j])
										.subtract(octonionUnits[k])
										.sum(octonionUnits[l])
										.divide(octonionTwo));
								aEven.add(octonionUnits[i]
										.sum(octonionUnits[j])
										.sum(octonionUnits[k])
										.subtract(octonionUnits[l])
										.divide(octonionTwo));
								aEven.add(octonionUnits[i]
										.subtract(octonionUnits[j])
										.sum(octonionUnits[k])
										.subtract(octonionUnits[l])
										.divide(octonionTwo));
								aEven.add(octonionUnits[i]
										.sum(octonionUnits[j])
										.subtract(octonionUnits[k])
										.subtract(octonionUnits[l])
										.divide(octonionTwo));
								aEven.add(octonionUnits[i]
										.subtract(octonionUnits[j])
										.subtract(octonionUnits[k])
										.subtract(octonionUnits[l])
										.divide(octonionTwo));
								cnt += 8;

								for (int m = 0; m < 8; m++) {
									aEven.add(aEven.get(m + cnt - 8).multiply(
											octonionNegativeOne));
								}
								cnt += 8;

								if (SNAPSHOT_DEBUG_OUTPUT) {
									H.sopln("Dixon A^even Snapshot " + cnt
											+ "/240:");
									H.sopln("  "
											+ aEven.get(cnt - 1)
													.toVectorString());
								}

								if (HEAVY_DEBUG_OUTPUT) {
									for (int m = 0; m < 16; m++) {
										H.sopln("  "
												+ aEven.get(m + cnt - 16)
														.toVectorString());
									}
								}

							}

						}
					}
				}
			}

			if (cnt != 240)
				throw new Exception(
						"Generation of E8 lattice inner shell failed expected members final test. Check code.");

			H.sopln("Generating E8 lattice inner shell (Dixon A^odd)");
			aOdd = new ArrayList<BigVectorOctonion>();

			// (1) get +-(e_i)+-(e_j) with distinct i, j (112 components)
			cnt = 0;
			for (int i = 0; i < 7; i++) {
				for (int j = i + 1; j < 8; j++) {
					aOdd.add(octonionUnits[i].sum(octonionUnits[j]));
					aOdd.add(octonionUnits[i].sum(octonionNegativeUnits[j]));
					aOdd.add(octonionNegativeUnits[i]
							.sum(octonionNegativeUnits[j]));
					aOdd.add(octonionNegativeUnits[i].sum(octonionUnits[j]));

					if ((SNAPSHOT_DEBUG_OUTPUT && ((i % 11) == 0))
							|| HEAVY_DEBUG_OUTPUT) {
						if (SNAPSHOT_DEBUG_OUTPUT)
							H.sopln("Dixon A^odd Snapshot " + (cnt + 1)
									+ "/240:");
						H.sopln("  " + aOdd.get(cnt).toVectorString());
						H.sopln("  " + aOdd.get(cnt + 1).toVectorString());
						H.sopln("  " + aOdd.get(cnt + 2).toVectorString());
						H.sopln("  " + aOdd.get(cnt + 3).toVectorString());
					}

					cnt += 4;
				}
			}

			if (cnt != 112)
				throw new Exception(
						"Generation of E8 lattice inner shell failed expected members interim test. Check code.");

			// (2) get (1/2)(+-1+-(e_1)...+-(e_7)) with odd number of minus
			// signs

			for (int i = 0; i < 256; i++) {

				numMinus = 0;
				thisOctonion = BigVectorOctonion.ZERO;
				binaryString = "0000000" + Integer.toBinaryString(i);
				binaryString = binaryString.substring(
						binaryString.length() - 8, binaryString.length());

				for (int j = 0; j < 8; j++) {

					if (binaryString.charAt(j) == '0') {
						numMinus++;
						thisOctonion = thisOctonion.subtract(octonionUnits[j]);
					} else {
						thisOctonion = thisOctonion.sum(octonionUnits[j]);
					}

				}

				if ((numMinus % 2) == 0) {
					// even number of minus signs; divide by 2 and add to E8
					// lattice
					aOdd.add(thisOctonion.divide(octonionTwo));

					if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 11) == 0))
							|| HEAVY_DEBUG_OUTPUT) {
						if (SNAPSHOT_DEBUG_OUTPUT)
							H.sopln("Dixon A^odd Snapshot " + (cnt + 1)
									+ "/240:");
						H.sopln("  " + aOdd.get(cnt).toVectorString());
					}

					cnt++;
				}

			}

			if (cnt != 240)
				throw new Exception(
						"Generation of E8 lattice inner shell failed expected members final test. Check code.");

		}

		if (CALCULATE_WILSON) {

			H.sopln("Generating E8 lattice inner shell (Wilson L)");
			e8L = new ArrayList<BigVectorOctonion>();

			// (1) get +-(e_i)+-(e_j) with distinct i, j (112 components)
			cnt = 0;
			for (int i = 0; i < 7; i++) {
				for (int j = i + 1; j < 8; j++) {
					e8L.add(octonionUnits[i].sum(octonionUnits[j]));
					e8L.add(octonionUnits[i].sum(octonionNegativeUnits[j]));
					e8L.add(octonionNegativeUnits[i]
							.sum(octonionNegativeUnits[j]));
					e8L.add(octonionNegativeUnits[i].sum(octonionUnits[j]));

					if ((SNAPSHOT_DEBUG_OUTPUT && ((i % 11) == 0))
							|| HEAVY_DEBUG_OUTPUT) {
						if (SNAPSHOT_DEBUG_OUTPUT)
							H.sopln("Wilson L Snapshot " + (cnt + 1) + "/240:");
						H.sopln("  " + e8L.get(cnt).toVectorString());
						H.sopln("  " + e8L.get(cnt + 1).toVectorString());
						H.sopln("  " + e8L.get(cnt + 2).toVectorString());
						H.sopln("  " + e8L.get(cnt + 3).toVectorString());
					}

					cnt += 4;
				}
			}

			if (cnt != 112)
				throw new Exception(
						"Generation of E8 lattice inner shell failed expected members interim test. Check code.");

			// (2) get (1/2)(+-1+-(e_1)...+-(e_7)) with odd number of minus
			// signs
			for (int i = 0; i < 256; i++) {

				numMinus = 0;
				thisOctonion = BigVectorOctonion.ZERO;
				binaryString = "0000000" + Integer.toBinaryString(i);
				binaryString = binaryString.substring(
						binaryString.length() - 8, binaryString.length());

				for (int j = 0; j < 8; j++) {

					if (binaryString.charAt(j) == '0') {
						numMinus++;
						thisOctonion = thisOctonion.subtract(octonionUnits[j]);
					} else {
						thisOctonion = thisOctonion.sum(octonionUnits[j]);
					}

				}

				if ((numMinus % 2) == 1) {
					// odd number of minus signs; divide by 2 and add to E8
					// lattice
					e8L.add(thisOctonion.divide(octonionTwo));

					if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 11) == 0))
							|| HEAVY_DEBUG_OUTPUT) {
						if (SNAPSHOT_DEBUG_OUTPUT)
							H.sopln("Wilson L Snapshot " + (cnt + 1) + "/240:");
						H.sopln("  " + e8L.get(cnt).toVectorString());
					}

					cnt++;
				}

			}

			if (cnt != 240)
				throw new Exception(
						"Generation of E8 lattice inner shell failed expected members final test. Check code.");

			H.sopln("Generating E8 L * s");
			e8L_s = new ArrayList<BigVectorOctonion>();
			for (cnt = 0; cnt < 240; cnt++) {
				e8L_s.add(e8L.get(cnt).multiply(s));
				if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 11) == 0))
						|| HEAVY_DEBUG_OUTPUT) {
					if (SNAPSHOT_DEBUG_OUTPUT)
						H.sopln("E8 L * s Snapshot " + (cnt + 1) + "/240:");
					H.sopln("  " + e8L_s.get(cnt).toVectorString());
				}
			}

			H.sopln("Generating E8 L * \\overbar{ s }");
			e8L_sBar = new ArrayList<BigVectorOctonion>();
			for (cnt = 0; cnt < 240; cnt++) {
				e8L_sBar.add(e8L.get(cnt).multiply(sBar));
				if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 11) == 0))
						|| HEAVY_DEBUG_OUTPUT) {
					if (SNAPSHOT_DEBUG_OUTPUT)
						H.sopln("E8 L * \\overbar{ s } Snapshot " + (cnt + 1)
								+ "/240:");
					H.sopln("  " + e8L_sBar.get(cnt).toVectorString());
				}
			}

			H.sopln("Generating E8 L * 2");
			e8L_2 = new ArrayList<BigVectorOctonion>();
			for (cnt = 0; cnt < 240; cnt++) {
				e8L_2.add(e8L.get(cnt).multiply(octonionTwo));
				if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 11) == 0))
						|| HEAVY_DEBUG_OUTPUT) {
					if (SNAPSHOT_DEBUG_OUTPUT)
						H.sopln("E8 L * 2 Snapshot " + (cnt + 1) + "/240:");
					H.sopln("  " + e8L_2.get(cnt).toVectorString());
				}
			}

			H.sopln("Generating E8 L * s * e_j (with j=0..7)");
			e8L_s_j = new ArrayList<BigVectorOctonion>();
			cnt = 0;
			for (int i = 0; i < 240; i++) {
				for (int j = 0; j < 8; j++) {
					e8L_s_j.add(e8L.get(i).multiply(s)
							.multiply(octonionUnits[j]));
					e8L_s_j.add(e8L.get(i).multiply(s)
							.multiply(octonionNegativeUnits[j]));
					if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 173) == 0))
							|| HEAVY_DEBUG_OUTPUT) {
						if (SNAPSHOT_DEBUG_OUTPUT)
							H.sopln("E8 L * s * e_j Snapshot " + (cnt + 1)
									+ "/" + (240 * 16) + ":");
						H.sopln("  " + e8L_s_j.get(cnt).toVectorString());
						H.sopln("  " + e8L_s_j.get(cnt + 1).toVectorString());
					}
					cnt += 2;
				}
			}
			if (cnt != (240 * 16)) {
				throw new Exception("E8 L * s * e_j count " + (240 * 16)
						+ " expected, but was " + cnt + "; check code");
			}

			H.sopln("Generating E8 L * sBar * e_j (with j=0..7)");
			e8L_sBar_j = new ArrayList<BigVectorOctonion>();
			cnt = 0;
			for (int i = 0; i < 240; i++) {
				for (int j = 0; j < 8; j++) {
					e8L_sBar_j.add(e8L.get(i).multiply(sBar)
							.multiply(octonionUnits[j]));
					e8L_sBar_j.add(e8L.get(i).multiply(sBar)
							.multiply(octonionNegativeUnits[j]));
					if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 173) == 0))
							|| HEAVY_DEBUG_OUTPUT) {
						if (SNAPSHOT_DEBUG_OUTPUT)
							H.sopln("E8 L * sBar * e_j Snapshot " + (cnt + 1)
									+ "/" + (240 * 16) + ":");
						H.sopln("  " + e8L_sBar_j.get(cnt).toVectorString());
						H.sopln("  " + e8L_sBar_j.get(cnt + 1).toVectorString());
					}
					cnt += 2;
				}
			}

			H.sopln("Generating E8 L * e_j (with j=0..7)");
			e8L_j = new ArrayList<BigVectorOctonion>();
			cnt = 0;
			for (int i = 0; i < 240; i++) {
				for (int j = 0; j < 8; j++) {
					e8L_j.add(e8L.get(i).multiply(octonionUnits[j]));
					e8L_j.add(e8L.get(i).multiply(octonionNegativeUnits[j]));
					if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 173) == 0))
							|| HEAVY_DEBUG_OUTPUT) {
						if (SNAPSHOT_DEBUG_OUTPUT)
							H.sopln("E8 L * e_j Snapshot " + (cnt + 1) + "/"
									+ (240 * 16) + ":");
						H.sopln("  " + e8L_j.get(cnt).toVectorString());
						H.sopln("  " + e8L_j.get(cnt + 1).toVectorString());
					}
					cnt += 2;
				}
			}

			H.sopln("Generating E8 ( L * e_j ) * e_k (with j,k=0..7)");
			long testStartTime = (new Date()).getTime();
			e8L_j_k = new ArrayList<BigVectorOctonion>();
			cnt = 0;
			BigVectorOctonion leftFactor;
			for (int i = 0; i < 240; i++) {
				for (int j = 0; j < 16; j++) {
					leftFactor = e8L_j.get(i * 16 + j);
					for (int k = 0; k < 8; k++) {
						e8L_j_k.add(leftFactor.multiply(octonionUnits[k]));
						e8L_j_k.add(leftFactor
								.multiply(octonionNegativeUnits[k]));
						if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 2765) == 0))
								|| HEAVY_DEBUG_OUTPUT) {
							if (SNAPSHOT_DEBUG_OUTPUT)
								H.sopln("E8 ( L * e_j ) * e_k Snapshot "
										+ (cnt + 1) + "/" + (240 * 16 * 16)
										+ ":");
							H.sopln("  " + e8L_j_k.get(cnt).toVectorString());
							H.sopln("  "
									+ e8L_j_k.get(cnt + 1).toVectorString());
						}
						cnt += 2;
					}
				}
				if ((i % 24) == 23)
					H.sopElapsed(testStartTime, i, 240);
			}
			if (cnt != (240 * 16 * 16)) {
				throw new Exception("( E8 L * e_j ) * e_k count "
						+ (240 * 16 * 16) + " expected, but was " + cnt
						+ "; check code");
			}

		}

		if (CALCULATE_DIXON) {

			H.sopln("Generating Leech lattice [Dixon]");
			leechDixon = new ArrayList<BigVectorOctonionTriple>();
			cnt = 0;
			for (int i = 0; i < 240; i++) {
				leechDixon.add(new BigVectorOctonionTriple(aOdd.get(i)
						.multiply(octonionTwo), BigVectorOctonion.ZERO,
						BigVectorOctonion.ZERO));
				leechDixon.add(new BigVectorOctonionTriple(
						BigVectorOctonion.ZERO, aOdd.get(i).multiply(
								octonionTwo), BigVectorOctonion.ZERO));
				leechDixon.add(new BigVectorOctonionTriple(
						BigVectorOctonion.ZERO, BigVectorOctonion.ZERO, aOdd
								.get(i).multiply(octonionTwo)));
				if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 305) == 0))
						|| HEAVY_DEBUG_OUTPUT) {
					if (SNAPSHOT_DEBUG_OUTPUT)
						H.sopln("[Leech Dixon] Snapshot " + (cnt + 1) + "/"
								+ 196560 + ":");
					H.sopln("  " + leechDixon.get(cnt).toVectorString());
					H.sopln("  " + leechDixon.get(cnt + 1).toVectorString());
					H.sopln("  " + leechDixon.get(cnt + 2).toVectorString());
				}
				cnt += 3;
			}
			for (int i = 0; i < 240; i++) {
				for (int j = 0; j < 8; j++) {
					leechDixon.add(new BigVectorOctonionTriple(aEven.get(i)
							.multiply(octonionTwo), octonionUnits[j].multiply(
							aEven.get(i)).multiply(octonionTwo),
							BigVectorOctonion.ZERO));
					leechDixon.add(new BigVectorOctonionTriple(aEven.get(i)
							.multiply(octonionTwo), octonionNegativeUnits[j]
							.multiply(aEven.get(i)).multiply(octonionTwo),
							BigVectorOctonion.ZERO));
					leechDixon.add(new BigVectorOctonionTriple(octonionUnits[j]
							.multiply(aEven.get(i)).multiply(octonionTwo),
							BigVectorOctonion.ZERO, aEven.get(i).multiply(
									octonionTwo)));
					leechDixon.add(new BigVectorOctonionTriple(
							octonionNegativeUnits[j].multiply(aEven.get(i))
									.multiply(octonionTwo),
							BigVectorOctonion.ZERO, aEven.get(i).multiply(
									octonionTwo)));
					leechDixon.add(new BigVectorOctonionTriple(
							BigVectorOctonion.ZERO, aEven.get(i).multiply(
									octonionTwo), octonionUnits[j].multiply(
									aEven.get(i)).multiply(octonionTwo)));
					leechDixon.add(new BigVectorOctonionTriple(
							BigVectorOctonion.ZERO, aEven.get(i).multiply(
									octonionTwo), octonionNegativeUnits[j]
									.multiply(aEven.get(i)).multiply(
											octonionTwo)));

					if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 305) == 0))
							|| HEAVY_DEBUG_OUTPUT) {
						if (SNAPSHOT_DEBUG_OUTPUT)
							H.sopln("[Leech Dixon] Snapshot " + (cnt + 1) + "/"
									+ 196560 + ":");
						H.sopln("  " + leechDixon.get(cnt).toVectorString());
						H.sopln("  " + leechDixon.get(cnt + 1).toVectorString());
						H.sopln("  " + leechDixon.get(cnt + 2).toVectorString());
						H.sopln("  " + leechDixon.get(cnt + 3).toVectorString());
						H.sopln("  " + leechDixon.get(cnt + 4).toVectorString());
						H.sopln("  " + leechDixon.get(cnt + 5).toVectorString());
					}
					cnt += 6;
				}
			}
			for (int i = 0; i < 240; i++) {
				for (int j = 0; j < 8; j++) {
					for (int k = 0; k < 8; k++) {
						leechDixon.add(new BigVectorOctonionTriple(aOdd.get(i),
								octonionUnits[j].multiply(aOdd.get(i)),
								octonionUnits[j].multiply(l0)
										.multiply(octonionUnits[k])
										.multiply(aOdd.get(i))));
						leechDixon.add(new BigVectorOctonionTriple(
								octonionUnits[j].multiply(aOdd.get(i)),
								octonionUnits[j].multiply(l0)
										.multiply(octonionUnits[k])
										.multiply(aOdd.get(i)), aOdd.get(i)));
						leechDixon.add(new BigVectorOctonionTriple(
								octonionUnits[j].multiply(l0)
										.multiply(octonionUnits[k])
										.multiply(aOdd.get(i)), aOdd.get(i),
								octonionUnits[j].multiply(aOdd.get(i))));

						leechDixon.add(new BigVectorOctonionTriple(aOdd.get(i),
								octonionNegativeUnits[j].multiply(aOdd.get(i)),
								octonionNegativeUnits[j].multiply(l0)
										.multiply(octonionUnits[k])
										.multiply(aOdd.get(i))));
						leechDixon.add(new BigVectorOctonionTriple(
								octonionNegativeUnits[j].multiply(aOdd.get(i)),
								octonionNegativeUnits[j].multiply(l0)
										.multiply(octonionUnits[k])
										.multiply(aOdd.get(i)), aOdd.get(i)));
						leechDixon
								.add(new BigVectorOctonionTriple(
										octonionNegativeUnits[j].multiply(l0)
												.multiply(octonionUnits[k])
												.multiply(aOdd.get(i)), aOdd
												.get(i),
										octonionNegativeUnits[j].multiply(aOdd
												.get(i))));

						leechDixon.add(new BigVectorOctonionTriple(aOdd.get(i),
								octonionUnits[j].multiply(aOdd.get(i)),
								octonionUnits[j].multiply(l0)
										.multiply(octonionNegativeUnits[k])
										.multiply(aOdd.get(i))));
						leechDixon.add(new BigVectorOctonionTriple(
								octonionUnits[j].multiply(aOdd.get(i)),
								octonionUnits[j].multiply(l0)
										.multiply(octonionNegativeUnits[k])
										.multiply(aOdd.get(i)), aOdd.get(i)));
						leechDixon.add(new BigVectorOctonionTriple(
								octonionUnits[j].multiply(l0)
										.multiply(octonionNegativeUnits[k])
										.multiply(aOdd.get(i)), aOdd.get(i),
								octonionUnits[j].multiply(aOdd.get(i))));

						leechDixon.add(new BigVectorOctonionTriple(aOdd.get(i),
								octonionNegativeUnits[j].multiply(aOdd.get(i)),
								octonionNegativeUnits[j].multiply(l0)
										.multiply(octonionNegativeUnits[k])
										.multiply(aOdd.get(i))));
						leechDixon.add(new BigVectorOctonionTriple(
								octonionNegativeUnits[j].multiply(aOdd.get(i)),
								octonionNegativeUnits[j].multiply(l0)
										.multiply(octonionNegativeUnits[k])
										.multiply(aOdd.get(i)), aOdd.get(i)));
						leechDixon
								.add(new BigVectorOctonionTriple(
										octonionNegativeUnits[j]
												.multiply(l0)
												.multiply(
														octonionNegativeUnits[k])
												.multiply(aOdd.get(i)), aOdd
												.get(i),
										octonionNegativeUnits[j].multiply(aOdd
												.get(i))));

						if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 71) == 0))
								|| HEAVY_DEBUG_OUTPUT) {
							if (SNAPSHOT_DEBUG_OUTPUT)
								H.sopln("[Leech Dixon] Snapshot " + (cnt + 1)
										+ "/" + 196560 + ":");
							for (int m = 0; m < 12; m++) {
								H.sopln("  "
										+ leechDixon.get(cnt + m)
												.toVectorString());
							}
						}
						cnt += 12;
					}
				}
			}
			if (cnt != 196560) {
				throw new Exception("Leech lattice inner shell count 196560"
						+ " expected, but was " + cnt + "; check code");
			}

		}

		if (CALCULATE_WILSON) {

			H.sopln("Generating Leech lattice [Wilson]");
			leechWilson = new ArrayList<BigVectorOctonionTriple>();
			cnt = 0;
			for (int i = 0; i < 240; i++) {
				leechWilson.add(new BigVectorOctonionTriple(e8L_2.get(i),
						BigVectorOctonion.ZERO, BigVectorOctonion.ZERO));
				leechWilson.add(new BigVectorOctonionTriple(
						BigVectorOctonion.ZERO, e8L_2.get(i),
						BigVectorOctonion.ZERO));
				leechWilson.add(new BigVectorOctonionTriple(
						BigVectorOctonion.ZERO, BigVectorOctonion.ZERO, e8L_2
								.get(i)));
				if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 2765) == 0))
						|| HEAVY_DEBUG_OUTPUT) {
					if (SNAPSHOT_DEBUG_OUTPUT)
						H.sopln("[Leech Wilson] Snapshot " + (cnt + 1) + "/"
								+ 196560 + ":");
					H.sopln("  " + leechWilson.get(cnt).toVectorString());
					H.sopln("  " + leechWilson.get(cnt + 1).toVectorString());
					H.sopln("  " + leechWilson.get(cnt + 2).toVectorString());
				}
				cnt += 3;
			}
			for (int i = 0; i < 240; i++) {
				for (int j = 0; j < 16; j++) {
					leechWilson.add(new BigVectorOctonionTriple(
							e8L_sBar.get(i), e8L_sBar_j.get(i * 16 + j),
							BigVectorOctonion.ZERO));
					leechWilson.add(new BigVectorOctonionTriple(
							BigVectorOctonion.ZERO, e8L_sBar.get(i), e8L_sBar_j
									.get(i * 16 + j)));
					leechWilson.add(new BigVectorOctonionTriple(e8L_sBar_j
							.get(i * 16 + j), BigVectorOctonion.ZERO, e8L_sBar
							.get(i)));
					if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 2765) == 0))
							|| HEAVY_DEBUG_OUTPUT) {
						if (SNAPSHOT_DEBUG_OUTPUT)
							H.sopln("[Leech Wilson] Snapshot " + (cnt + 1)
									+ "/" + 196560 + ":");
						H.sopln("  " + leechWilson.get(cnt).toVectorString());
						H.sopln("  "
								+ leechWilson.get(cnt + 1).toVectorString());
						H.sopln("  "
								+ leechWilson.get(cnt + 2).toVectorString());
					}
					cnt += 3;
				}
			}
			for (int i = 0; i < 240; i++) {
				for (int j = 0; j < 16; j++) {
					for (int k = 0; k < 16; k++) {
						leechWilson.add(new BigVectorOctonionTriple(e8L_s_j
								.get(i * 16 + j), e8L_j.get(i * 16 + k),
								e8L_j_k.get(i * 256 + j * 16 + k)));
						leechWilson.add(new BigVectorOctonionTriple(e8L_j_k
								.get(i * 256 + j * 16 + k), e8L_s_j.get(i * 16
								+ j), e8L_j.get(i * 16 + k)));
						leechWilson.add(new BigVectorOctonionTriple(e8L_j.get(i
								* 16 + k), e8L_j_k.get(i * 256 + j * 16 + k),
								e8L_s_j.get(i * 16 + j)));
						if ((SNAPSHOT_DEBUG_OUTPUT && ((cnt % 2765) == 0))
								|| HEAVY_DEBUG_OUTPUT) {
							if (SNAPSHOT_DEBUG_OUTPUT)
								H.sopln("[Leech Wilson] Snapshot " + (cnt + 1)
										+ "/" + 196560 + ":");
							H.sopln("  "
									+ leechWilson.get(cnt).toVectorString());
							H.sopln("  "
									+ leechWilson.get(cnt + 1).toVectorString());
							H.sopln("  "
									+ leechWilson.get(cnt + 2).toVectorString());
						}
						cnt += 3;
					}
				}
			}
			if (cnt != 196560) {
				throw new Exception("Leech lattice inner shell count 196560"
						+ " expected, but was " + cnt + "; check code");
			}

		}

		H.sopln("Initializing done.");
		H.sopln("-----------------------------------------------");

	}

	//
	// -----------------------------------------------------------
	// Below are some tests to qualify the algebra
	// (alternative, free from null-spaces).
	// -----------------------------------------------------------
	//

	private static void runInternalTests() throws Exception {
		H.sopln("Running some tests of the multiplication table:");

		H.sopln("Octonions: 1+2=3");
		H.sopln(octonionOne.sum(octonionTwo).toScript() + " = "
				+ (octonionOne.sum(octonionTwo)).toVectorString());

		BigVectorOctonion octonionNeg2Sbar = sBar.multiply(octonionTwo)
				.multiply(octonionNegativeOne);
		H.sopln("Octonion -2sBar 1 + i_1 + ... + i_7 = "
				+ octonionNeg2Sbar.toScript() + " = "
				+ octonionNeg2Sbar.toVectorString());

		H.sopln("Multiplication table when multiplying -2sBar with i_1, ..., i_7:");

		for (int i = 0; i < 8; i++) {
			H.sopln("["
					+ octonionUnits[i].toScript()
					+ "] "
					+ octonionNeg2Sbar.multiply(octonionUnits[i]).toScript()
					+ " = "
					+ (octonionNeg2Sbar.multiply(octonionUnits[i]))
							.toVectorString());
		}

		H.sopln("Multiplication table when multiplying -sBar with -i_1, ..., -i_7:");

		for (int i = 0; i < 8; i++) {
			H.sopln("["
					+ octonionNegativeUnits[i].toScript()
					+ "] "
					+ octonionNeg2Sbar.multiply(octonionNegativeUnits[i])
							.toScript()
					+ " = "
					+ (octonionNeg2Sbar.multiply(octonionNegativeUnits[i]))
							.toVectorString());
		}

		if (CALCULATE_DIXON) {

			if (aOdd.contains(l0)) {
				H.sopln("l0 = " + l0.toVectorString()
						+ " is contained in E8 A^odd [Dixon] OK");
			} else {
				throw new Exception("l0 = " + l0.toVectorString()
						+ " is not contained in E8 aOdd; check lookup logic");
			}

			H.sopln("Checking l_0 * A^even in A^odd [Dixon] ");
			for (int i = 0; i < 240; i++) {
				BigVectorOctonion prod = l0.multiply(aEven.get(i));
				if (!aOdd.contains(prod))
					throw new Exception("Product l0 * aEven(" + i + ") = "
							+ prod.toVectorString() + " not in aOdd. l0 = "
							+ l0.toVectorString() + ", aEven(" + i + ") = "
							+ aEven.get(i).toVectorString());
			}

			H.sopln("Checking (l_0)^(-1) * A^odd in A^even [Dixon] ");
			BigVectorOctonion l0_inv = octonionOne.divide(l0);
			for (int i = 0; i < 240; i++) {
				BigVectorOctonion prod = l0_inv.multiply(aOdd.get(i));
				if (!aEven.contains(prod))
					throw new Exception("Product l0_inv * aOdd(" + i
							+ ") not in aEven. aOdd(" + i + ") = "
							+ aOdd.get(i).toVectorString());
			}

		}

		if (CALCULATE_WILSON) {

			if (e8L.contains(s)) {
				H.sopln("s = " + s.toVectorString()
						+ " is contained in E8 L [Wilson] OK");
			} else {
				throw new Exception("s = " + s.toVectorString()
						+ " is not contained in E8 L; check lookup logic");
			}

			if (e8L_s.contains(s)) {
				throw new Exception(
						"s = "
								+ s.toVectorString()
								+ " is contained in (E8 L)s but it shouldn't; check lookup logic");
			} else {
				H.sopln("s = " + s.toVectorString()
						+ " is not contained in (E8 L)s [Wilson] OK");
			}

			if (e8L_sBar.contains(s)) {
				throw new Exception(
						"s = "
								+ s.toVectorString()
								+ " is contained in (E8 L)\\overbar{ s } but it shouldn't; check lookup logic");
			} else {
				H.sopln("s = "
						+ s.toVectorString()
						+ " is not contained in (E8 L)\\overbar{ s } [Wilson] OK");
			}

		}

		testScalarProduct();

		if (!SKIP_INTERNAL_TESTS_FOR_DUPLICATES) {
			testDuplicatesFinderAlgorithm();
			if (CALCULATE_DIXON) {
				testListForDuplicates(aEven, "aEven", 9999);
				testListForDuplicates(aOdd, "aOdd", 9999);
				testListForDuplicates(leechDixon,
						"Leech lattice [Dixon] inner shell", 10000);
				testLeechLatticeNorm(leechDixon, "Dixon");
			}
			if (CALCULATE_WILSON) {
				testListForDuplicates(e8L, "e8L", 9999);
				testListForDuplicates(e8L_s, "e8L_s", 9999);
				testListForDuplicates(e8L_sBar, "e8L_sBar", 9999);
				testListForDuplicates(e8L_2, "e8L_2", 9999);
				testListForDuplicates(leechWilson,
						"Leech lattice [Wilson] inner shell", 10000);
				testLeechLatticeNorm(leechWilson, "Wilson");
			}
		}
		testAlternativityAndInvertibility();

		H.sopln("Tests completed OK.");
		H.sopln("-----------------------------------------------");

	}

	private static void testScalarProduct() throws Exception {
		H.sopln("Testing scalar product");
		long[] coefs1 = new long[8];
		long[] coefs2 = new long[8];
		int thisResult;
		int expectedResult;
		long testStartTime = (new Date()).getTime();
		int printStatusEvery = SCALAR_PRODUCT_TESTS_COUNT / 10;

		for (int i = 0; i < SCALAR_PRODUCT_TESTS_COUNT; i++) {

			expectedResult = 0;

			for (int j = 0; j < 8; j++) {
				coefs1[j] = Math.round(Math.random() * 11.) - 5;
				coefs2[j] = Math.round(Math.random() * 11.) - 5;
				expectedResult += coefs1[j] * coefs2[j];
			}

			BigVectorOctonion factor1 = new BigVectorOctonion(coefs1);
			BigVectorOctonion factor2 = new BigVectorOctonion(coefs2);
			BigVectorOctonion scalarProduct = factor1.scalarProduct(factor2);
			thisResult = scalarProduct.getRe().numerator().intValue();

			if (thisResult != expectedResult) {
				throw new Exception("Scalar product of "
						+ factor1.toVectorString() + " and "
						+ factor2.toVectorString() + " was expected to be "
						+ expectedResult + ", but came back as " + thisResult
						+ ". Check scalarProduct implementation.");
			}

			if ((i % printStatusEvery) == (printStatusEvery - 1))
				H.sopElapsed(testStartTime, i, SCALAR_PRODUCT_TESTS_COUNT);

		}

	}

	private static void testDuplicatesFinderAlgorithm() throws Exception {
		H.sopln("Testing duplicate finder algorithm for octonions");
		ArrayList<BigVectorOctonion> duplicatesTestOctonion = new ArrayList<BigVectorOctonion>();
		BigVectorOctonion dup1 = new BigVectorOctonion("2");
		BigVectorOctonion dup2 = new BigVectorOctonion("2");
		duplicatesTestOctonion.add(dup1);
		duplicatesTestOctonion.add(dup2); // duplicate
		testDuplicatesFinderAlgorithm(duplicatesTestOctonion);

		H.sopln("Testing duplicate finder algorithm for octonion triples");
		ArrayList<BigVectorOctonionTriple> duplicatesTestOctonionTriple = new ArrayList<BigVectorOctonionTriple>();
		BigVectorOctonionTriple dupT1 = new BigVectorOctonionTriple(dup1, dup2,
				dup1);
		BigVectorOctonionTriple dupT2 = new BigVectorOctonionTriple(dup1, dup2,
				dup1);
		duplicatesTestOctonionTriple.add(dupT1);
		duplicatesTestOctonionTriple.add(dupT2); // duplicate
		testDuplicatesFinderAlgorithm(duplicatesTestOctonionTriple);
	}

	private static void testDuplicatesFinderAlgorithm(ArrayList duplicatesTest)
			throws Exception {

		if (!duplicatesTest.get(0).equals(duplicatesTest.get(1)))
			throw new Exception(
					"Objects are equal but not recognized. Check equals implementation in "
							+ duplicatesTest.get(0).getClass()
									.getCanonicalName());
		boolean exceptionCaught = false;
		try {
			testListForDuplicates(duplicatesTest, "duplicates test", 9999);
		} catch (Exception e) {
			H.sopln("Exception caught as is should, test OK.");
			exceptionCaught = true;
		}
		if (!exceptionCaught)
			throw new Exception("Duplicates list did not throw exception. "
					+ "Check testListForDuplicates code. "
					+ "Check hashCode and equals implementation in "
					+ duplicatesTest.get(0).getClass().getCanonicalName());

	}

	private static void testListForDuplicates(ArrayList list,
			String description, int printUpdateEvery) throws Exception {

		H.sopln("Testing " + description + " ("
				+ list.getClass().getCanonicalName() + ") for duplicates");

		long testStartTime = (new Date()).getTime();
		HashSet testSet = new HashSet();

		for (int i = 0; i < list.size(); i++) {
			if (testSet.add(list.get(i)) == false) {
				throw new Exception("Number at index " + i
						+ " is duplicate. Check generating code.");
			}
			if ((i % printUpdateEvery) == (printUpdateEvery - 1))
				H.sopElapsed(testStartTime, i, list.size());
		}

		H.sopln("Test OK.");

	}

	private static void testAlternativityAndInvertibility() throws Exception {
		// runs through some tests to make sure the algebra is alternative
		BigVectorOctonion factor1;
		BigVectorOctonion factor2;
		BigVectorOctonion product1;
		BigVectorOctonion product2;
		BigVectorOctonion product3;
		BigVectorOctonion product4;
		int numEqual = 0;
		int numZero = 0;
		long testStartTime = (new Date()).getTime();

		H.sopln("Performing random tests for alternativity of the algebra");

		for (int i = 0; i < RANDOM_TESTS_COUNT; i++) {

			factor1 = BigVectorOctonion.ORAND(1);
			factor2 = BigVectorOctonion.ORAND(1);
			product1 = factor1.multiply(factor1.multiply(factor2));
			product2 = (factor1.multiply(factor1)).multiply(factor2);
			product3 = factor1.multiply(factor2.multiply(factor2));
			product4 = (factor1.multiply(factor2)).multiply(factor2);

			if (factor1.equals(factor2)) {
				numEqual++;
			}

			if (!(product1.equals(product2))) {
				throw new Exception(
						"Alternativity fault on test a(ab) = (aa)b. Check your algebra.");
			}

			if (!(product3.equals(product4))) {
				throw new Exception(
						"Alternativity fault on test a(bb) = (ab)b. Check your algebra.");
			}

			if (product1.equals(BigVectorOctonion.ZERO)) {
				numZero++;
				if (!(product2.equals(BigVectorOctonion.ZERO)
						&& product3.equals(BigVectorOctonion.ZERO)
						&& product4.equals(BigVectorOctonion.ZERO) && (factor1
						.equals(BigVectorOctonion.ZERO) || factor2
						.equals(BigVectorOctonion.ZERO)))) {
					throw new Exception("Nullspace fault. Check you algebra.");
				}
			}

			if ((i % (RANDOM_TESTS_COUNT / 100) == 0) || HEAVY_DEBUG_OUTPUT) {
				H.sopElapsed(testStartTime, i, RANDOM_TESTS_COUNT);
				if (SNAPSHOT_DEBUG_OUTPUT) {
					H.sopln("[alternativity test] Snapshot: just tested a(ab) = (aa)b, a(bb) = (ab)b for:");
					H.sopln("       a = " + factor1.toVectorString());
					H.sopln("       b = " + factor2.toVectorString());
					H.sopln("       Number of a == b so far: " + numEqual);
					H.sopln("       Number of a = 0 or b = 0 so far: "
							+ numZero);
				}
			}

		}

	}

	private static void testLeechLatticeNorm(
			ArrayList<BigVectorOctonionTriple> leech, String representation)
			throws Exception {

		BigRational eight = new BigRational("8");
		boolean isEight;
		BigRational thisNorm;
		int eightFound = 0;
		long testStartTime = (new Date()).getTime();

		H.sopln("Test whether E8 lattice inner shell [" + representation
				+ "] norm is 8");

		for (int i = 0; i < leech.size(); i++) {
			thisNorm = leech.get(i).norm();
			isEight = thisNorm.equals(eight);
			if (isEight) {
				eightFound++;
			} else {
				throw new Exception("Leech lattice [" + representation
						+ "] inner shell member "
						+ leech.get(i).toVectorString()
						+ " does not have norm 8. Check code.");
			}
			if ((i % 20000) == 19999)
				H.sopElapsed(testStartTime, i, 196560);
		}

		if (eightFound != 196560) {
			throw new Exception("Norm 8 was found " + eightFound
					+ " (should be 196560). Check code [" + representation
					+ "].");
		}

		H.sopln("Test OK (found norm 8: " + eightFound + " times in ["
				+ representation + "]).");
	}

}
