package lafa;

import edu.jas.arith.BigOctonion;
import edu.jas.arith.BigQuaternion;
import edu.jas.arith.BigRational;

import java.io.Serializable;

public class BigVectorOctonion implements Serializable {

	private static final long serialVersionUID = 1L;
	private BigOctonion o;
	private BigRational coefficients[];
	private int hashCode;

	public static BigVectorOctonion[] units = null;
	public static BigVectorOctonion[] negativeUnits;
	public static BigVectorOctonion ZERO;
	public static BigVectorOctonion NEGATIVE_ONE;
	public static BigVectorOctonion NAN;
	public static BigOctonion[] diagonalSignature;

	public BigVectorOctonion(String toScript) {
		this.o = new BigOctonion(toScript);
		initialize();
	}

	public BigVectorOctonion(String o0, String o1, String o2, String o3,
			String o4, String o5, String o6, String o7) {

		BigQuaternion quatR = new BigQuaternion(o0 + "i" + o1 + "j" + o2 + "k"
				+ o3);
		BigQuaternion quatI = new BigQuaternion(o4 + "i" + o5 + "j" + o6 + "k"
				+ o7);
		this.o = new BigOctonion(quatR, quatI);
		initialize();
	}

	public BigVectorOctonion(BigRational coefficients[]) {
		BigQuaternion quatR = new BigQuaternion(coefficients[0].toString()
				+ "i" + coefficients[1].toString() + "j"
				+ coefficients[2].toString() + "k" + coefficients[3].toString());
		BigQuaternion quatI = new BigQuaternion(coefficients[4].toString()
				+ "i" + coefficients[5].toString() + "j"
				+ coefficients[6].toString() + "k" + coefficients[7].toString());
		this.o = new BigOctonion(quatR, quatI);
		initialize();
	}

	public BigVectorOctonion(long coefficients[]) {
		BigQuaternion quatR = new BigQuaternion(coefficients[0] + "i"
				+ coefficients[1] + "j" + coefficients[2] + "k"
				+ coefficients[3]);
		BigQuaternion quatI = new BigQuaternion(coefficients[4] + "i"
				+ coefficients[5] + "j" + coefficients[6] + "k"
				+ coefficients[7]);
		this.o = new BigOctonion(quatR, quatI);
		initialize();
	}

	/**
	 * Private constructor; skips initialization of static variables
	 */
	private BigVectorOctonion(BigOctonion bigOctonion) {
		this.o = bigOctonion;
		initializeDiagonalSignature();
		calculateCoefficientsAndHashCode();
	}

	private void initialize() {

		// thread-safe first-time initialization of static variables
		if (units == null) {
			synchronized (this) {
				if (units == null) {
					BigOctonion negativeOne = new BigOctonion("-1");

					units = new BigVectorOctonion[8];
					negativeUnits = new BigVectorOctonion[8];
					for (int i = 0; i < 8; i++) {
						units[i] = new BigVectorOctonion(negativeOne
								.generators().get(i));
						negativeUnits[i] = new BigVectorOctonion(negativeOne
								.generators().get(i).multiply(negativeOne));
					}

					ZERO = new BigVectorOctonion(BigOctonion.ZERO);
					NEGATIVE_ONE = new BigVectorOctonion(negativeOne);
					initializeDiagonalSignature();
				}
			}
		}

		// per-instance initialization
		calculateCoefficientsAndHashCode();
	}

	private void initializeDiagonalSignature() {

		if (diagonalSignature == null) {
			synchronized (this) {
				if (diagonalSignature == null) {
					BigOctonion negativeOne = new BigOctonion("-1");
					diagonalSignature = new BigOctonion[8];
					diagonalSignature[0] = new BigOctonion("1");
					for (int i = 1; i < 8; i++) {
						diagonalSignature[i] = negativeOne.generators().get(i)
								.multiply(negativeOne);
					}
				}
			}
		}

	}

	private void calculateCoefficientsAndHashCode() {
		// This is the method that's currently (20 Sep 2013) missing from the
		// BigOctonion class, which makes it hard to extract octonion
		// coefficients for calculating scalar vector product (when embedding
		// the octonion into a vector space over the rationals).

		int hashCode = 0;
		coefficients = new BigRational[8];
		for (int i = 0; i < 8; i++) {
			coefficients[i] = (o.multiply(diagonalSignature[i])).getR().getRe();
			hashCode += coefficients[i].hashCode();
		}
		this.hashCode = hashCode;

	}

	public void setBigOctonion(BigOctonion bigOctonion) {
		this.o = bigOctonion;
		calculateCoefficientsAndHashCode();
	}

	public BigOctonion getBigOctonion() {
		return o;
	}

	public BigRational[] getCoefficients() {
		return coefficients;
	}

	public BigVectorOctonion multiply(BigVectorOctonion factor) {
		return new BigVectorOctonion(o.multiply(factor.getBigOctonion()));
	}

	public BigVectorOctonion divide(BigVectorOctonion divisor) {
		return new BigVectorOctonion(o.divide(divisor.getBigOctonion()));
	}

	public BigVectorOctonion sum(BigVectorOctonion addend) {
		return new BigVectorOctonion(o.sum(addend.getBigOctonion()));
	}

	public BigVectorOctonion subtract(BigVectorOctonion subtrahend) {
		return new BigVectorOctonion(o.subtract(subtrahend.getBigOctonion()));
	}

	public BigVectorOctonion norm() {
		return new BigVectorOctonion(o.norm());
	}

	public BigVectorOctonion scalarProduct(BigVectorOctonion factor) {
		BigRational scalarProduct = BigRational.ZERO;

		for (int i = 0; i < 8; i++) {
			scalarProduct = scalarProduct.sum(this.coefficients[i]
					.multiply(factor.coefficients[i]));
		}

		return new BigVectorOctonion(scalarProduct.toString());
	}

	public BigVectorOctonion swapCoefficient(int kirmseIndex) {
		BigRational newCoefficients[] = coefficients;
		BigRational tmp = coefficients[0];
		newCoefficients[0] = coefficients[kirmseIndex];
		newCoefficients[kirmseIndex] = tmp;
		return new BigVectorOctonion(newCoefficients);
	}

	public BigRational normRe() {
		return o.norm().getR().getRe();
	}

	public BigVectorOctonion conjugate() {
		return new BigVectorOctonion(o.conjugate());
	}

	public BigRational getRe() {
		return o.getR().getRe();
	}

	public static BigVectorOctonion ORAND(int range) {
		return new BigVectorOctonion(BigOctonion.ORAND(range));
	}

	public String toScript() {
		return o.toScript();
	}

	public String toVectorString() {
		String retS = "(";

		for (int i = 0; i < 8; i++) {
			if (i != 0)
				retS += ", ";
			retS += coefficients[i].toString();
		}

		return retS + ")";
	}

	@Override
	public boolean equals(Object compareTo) {

		if (compareTo == null)
			return false;
		if (compareTo.getClass() != BigVectorOctonion.class)
			return false;
		return ((BigVectorOctonion) compareTo).getBigOctonion().equals(o);

	}

	@Override
	public int hashCode() {
		return this.hashCode;
	}

}
