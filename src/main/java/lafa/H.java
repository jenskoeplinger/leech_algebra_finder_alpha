package lafa;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.File;
import java.io.FileReader;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

/**
 * Little helpers
 */
public class H {

	static long startTime = -1;

	public static void writeObjectToTextFile(Object myObject,
			String textFileName) throws IOException {
		// serializes the object into a BASE64-encoded string, then saves
		// that string to file

		sopln("Write file: " + textFileName + "...");

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(myObject);
		oos.flush();
		byte[] binary = baos.toByteArray();

		sopln("...Serialized object OK, converting to Base64 string...");
		
		String text = DatatypeConverter.printBase64Binary(binary);

		sopln("...output string created OK, writing file...");

		PrintWriter out = new PrintWriter(textFileName);
		out.println(text);
		out.close();

		sopln("...Done.");

	}

	public static Object readObjectFromTextFile(String textFileName)
			throws IOException, ClassNotFoundException {
		// reads BASE64-encoded string from file, then deserializes
		// it into a Java object

		sopln("Read file: " + textFileName + "...");

		String content = null;
		File file = new File(textFileName);
		try {
			FileReader reader = new FileReader(file);
			char[] chars = new char[(int) file.length()];
			reader.read(chars);
			content = new String(chars);
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		sopln("...file read ok, converting Base64 string to byte array...");

		byte[] binary = DatatypeConverter.parseBase64Binary(content);

		sopln("...byte array created ok, deserializing object...");

		ByteArrayInputStream bais = new ByteArrayInputStream(binary);
		ObjectInputStream ois = new ObjectInputStream(bais);
		Object myObject = (Object) ois.readObject();
		ois.close();

		sopln("...Done.");

		return myObject;

	}

	// "sop" = System.out.print
	public static void sop(String printS) {
		long thisTime = (new Date()).getTime();
		if (startTime < 0)
			startTime = thisTime;
		System.out.print("[" + timeS(thisTime - startTime) + "] " + printS);
	}

	public static void sopln(String printS) {
		sop(printS);
		System.out.println("");
	}

	// time in hour:minute:second:millisecond
	public static String timeS(long timeL) {
		long hourL = timeL / 3600000L;
		long minuteL = (timeL - (hourL * 3600000L)) / 60000L;
		long secondL = (timeL - (hourL * 3600000L + minuteL * 60000L)) / 1000L;
		long msS = timeL % 1000L;
		return "" + hourL + ":" + minuteL + ":" + secondL + ":" + msS;
	}

	// print elapsed time and completion estimate
	public static void sopElapsed(long testStartTime, int i, int listSize) {

		long elapsedTime = (new Date()).getTime() - testStartTime;
		double percentComplete = ((double) i) / ((double) listSize);
		double estdRemaining = (1. - percentComplete) * ((double) elapsedTime)
				/ (0.00001 + percentComplete);
		sopln("(" + (i + 1) + "/" + listSize + ") ["
				+ ((100 * (i + 1)) / listSize) + "%] time elapsed: "
				+ timeS(elapsedTime) + ", estimated time remaining: "
				+ timeS((long) estdRemaining));
	}

}
