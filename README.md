# Leech Lattice Algebra Finder (Alpha)

Jens Koeplinger (2013)

## About

Given two representations of the Leech lattice that each use an octonion triple (one by Geoffrey Dixon, one by Robert
Wilson), this code allows to quickly run tests whether a three-dimensional octonion-valued product in is closed on the
points closest to the origin of the lattice.

In the version here, it checks a certain 24-dimensional subalgebra of a representation of the 27-dimensional exceptional
Jordan algebra (which in turn is a subalgebra of octonion-valued 3x3 matrix algebra). It does not close on the Leech
lattice.

As I'm writing this (19 Feb 2022), no nondegenerate algebra product in 24 dimensions is known that is three-dimensional,
octonion-valued, and closed on the Leech lattice.
Here, "nondegenerate" means that there exists a bilinear form `B(x,y)` defined
on every `x,y` in `R^24` such that `B(x,y)=0` for every `x` implies `y=0`, and vice versa.

## Disclaimer

This is lab code, i.e., poorly written, hard to read, undocumented, using poor programming conventions, something I use
ad-hoc now and then. If you bet money on it, I will not join (or oppose) that bet.

## License

Public domain (no copyright; you would be foolish to copy it anyway ... a rewrite would be more prudent). Feel free to
pretend that aliens dictated it to you; though I'd enjoy it if you send me a note.

## References

* [Wilson] Robert A. Wilson, Octonions and the Leech lattice (2008/2009)
* [Dixon] Geoffrey M. Dixon, Division Algebras, Lattices, Physics, Windmill Tilting (2011)

(and related journal articles and preprints).
